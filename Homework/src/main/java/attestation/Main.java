package attestation;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        UserFile file = new UserFile("User.txt");
        UsersRepositoryFileImpl userApp = new UsersRepositoryFileImpl();
        Scanner scanner = new Scanner(System.in);
        String name1;
        String lastName1;
        int age1;
        String work1;
        System.out.println("Введите имя");
        name1 = scanner.nextLine();
        System.out.println("Введите фамилию");
        lastName1 = scanner.nextLine();
        System.out.println("Введите возраст");
        age1 = scanner.nextInt();
        scanner.nextLine();
        System.out.println("Вы работаете?");
        work1 = scanner.nextLine();
        User us = new User(name1, lastName1, age1, file.foo(work1));
        userApp.create(us);


        System.out.println("Введите id");
        System.out.println(userApp.findById(scanner.nextInt()));


        User user = new User("Илья", "Бобров", 44, true);
        userApp.update(user);


        System.out.println("Введите id для удаления");
        userApp.delete(scanner.nextInt());


    }
}
