package attestation;



import java.util.Objects;
import java.util.Random;

public class User {

    private int id;
    private String name;
    private String lastName;
    private int age;
    private boolean work;

    public User() {
    }

    public User(int id, String name, String lastName, int age, boolean work) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.work = work;
    }

    public User(String name, String lastName, int age, boolean work) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.work = work;
    }


    public int getId() {
        Random random = new Random();
        id = random.nextInt(1000);
        if (id == id) {
            id++;

        }
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isWork() {
        return work;
    }

    public void setWork(boolean work) {
        this.work = work;
    }

    @Override
    public String toString() {
        return id + "|"
                + name + "|"
                + lastName + "|"
                + age + "|"
                + work;


    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return getId() == user.getId() && getAge() == user.getAge() && isWork() == user.isWork() && Objects.equals(getName(), user.getName()) && Objects.equals(getLastName(), user.getLastName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getLastName(), getAge(), isWork());
    }
}