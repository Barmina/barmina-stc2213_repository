package attestation;

import java.io.*;
import java.util.Map;

public class UserFile {

    private final String fileName;

    public UserFile(String fileName) {
        this.fileName = fileName;


    }

    public void fileWriter(Map<Integer, User> map) {
        for (User user : map.values()) {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter("User.txt", false))) {
                writer.write(user.toString());
                writer.write("\n");
                writer.flush();



            } catch (IOException e) {
                System.out.println("Файл не найден");
            }
        }

    }


    public boolean foo(String str) {
        if (str.equalsIgnoreCase("да")) {
            return true;
        }
        return false;
    }


    public void fileReader(Map<Integer, User> map) {
        int id;
        String name2;
        String lastName2;
        int age2;
        boolean work2;
        String line = null;
        try (BufferedReader reader = new BufferedReader(new FileReader("User.txt"))) {
            while ((line = reader.readLine()) != null) {
                String[] info = line.split("\\|");
                id = Integer.parseInt(info[0]);
                name2 = info[1];
                lastName2 = info[2];
                age2 = Integer.parseInt(info[3]);
                work2 = Boolean.parseBoolean(info[4]);
                User user = new User(id, name2, lastName2, age2, work2);
                map.put(id, user);


            }


        } catch (IOException e) {
            System.out.println("Файл не найден");
        }

    }


}



