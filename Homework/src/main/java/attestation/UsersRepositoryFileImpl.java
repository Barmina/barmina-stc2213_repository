package attestation;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;



public class UsersRepositoryFileImpl implements UserMethods {
    Map<Integer, User> map = new HashMap<>();
    User user = new User();
    UserFile file = new UserFile("User.txt");

    @Override
    public User findById(int id) {
        file.fileReader(map);
        if (user == null) {
            System.out.println("Пользователь не найден");
        }
        return map.get(id);

    }

    @Override
    public void create(User user) {
        map.put(user.getId(), user);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("User.txt", true))) {
            writer.write(user.toString());
            writer.write("\n");
            writer.flush();



        } catch (IOException e) {
            System.out.println("Файл не найден");
        }
    }





    @Override
    public void update(User user) {
        file.fileReader(map);
        map.put(user.getId(), user);
        file.fileWriter(map);
    }




    @Override
    public void delete(int id) {
        file.fileReader(map);
        map.remove(id);
        file.fileWriter(map);



    }
}
