package Homework_12;

import java.util.HashMap;
import java.util.Map;

public class Collections {
    public static void main(String[] args) {
        String str = "На улице идёт сильный сильный дождь"; //Создаем строку с текстом
        Map<String,Integer> map = new HashMap<>(); //Создаем мапу
        for(String word : str.split(" ")){ //Перебираем элементы(именно пробелы с помощью split) нашей строки
            if(word.length() > 0){
                map.put(word, map.getOrDefault(word, 0) +1); //Заполняем нашу мапу, используя ее
                // метод(getOrDefault), который возвращает значение с сопоставленным ключом.
            }
        }
        System.out.println(map);
    }
}
