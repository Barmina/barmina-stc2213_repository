package Homework_10;


public class Sequence {
    public static int[] filter(int[] array, ByCondition condition) {
        int[] newArray = new int[array.length];
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                newArray[index] = array[i];
                index++;
            }
        }
        int[] arrayNew = new int[index];
        System.arraycopy(newArray, 0, arrayNew, 0, index);
        return arrayNew;

    }
}
