package Homework_10;

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        int[] arrayOne = new int[10];
        for (int i = 0; i < arrayOne.length; i++) {
            arrayOne[i] = random.nextInt(50);
        }
        ByCondition condition = number -> number % 2 == 0;
        int[] filterOne = Sequence.filter(arrayOne, condition);
        System.out.println("Проверка чисел на четность элемента");
        System.out.println(Arrays.toString(filterOne));

        ByCondition condition1 = (number) -> {
            int sum = 0;
            while (number > 0) {
                sum += number % 10;
                number /= 10;
            }
            return sum % 2 == 0;
        };
        int[] filterTwo = Sequence.filter(arrayOne,condition1);
        System.out.println("Значения, где сумма цифр элемента является четным числом.");
        System.out.println(Arrays.toString(filterTwo));
    }
}






